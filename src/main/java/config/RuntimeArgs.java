package config;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.RequiredArgsConstructor;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldDefaults;

import java.util.List;

//@Data
public class RuntimeArgs {
    //TODO - use singleton

    private static int crawlDepth;
    private static int maxParallelThreads;
    private static String downloadDest;
    private static String csvFilePath;
    private static String progressFilePath;
    private static List<String> mimeFormatsAllowed;

    public static int getCrawlDepth() { return crawlDepth; }
    public static void setCrawlDepth(int crawlDepth) { RuntimeArgs.crawlDepth = crawlDepth; }
    public static int getMaxParallelThreads() { return maxParallelThreads; }
    public static void setMaxParallelThreads(int maxParallelThreads) { RuntimeArgs.maxParallelThreads = maxParallelThreads; }
    public static String getDownloadDest() { return downloadDest; }
    public static void setDownloadDest(String downloadDest) { RuntimeArgs.downloadDest = downloadDest; }
    public static String getCsvFilePath() { return csvFilePath; }
    public static void setCsvFilePath(String csvFilePath) { RuntimeArgs.csvFilePath = csvFilePath; }
    public static String getProgressFilePath() { return progressFilePath; }
    public static void setProgressFilePath(String progressFilePath) { RuntimeArgs.progressFilePath = progressFilePath; }
    public static List<String> getMimeFormatsAllowed() { return mimeFormatsAllowed; }
    public static void setMimeFormatsAllowed(List<String> mimeFormatsAllowed) { RuntimeArgs.mimeFormatsAllowed = mimeFormatsAllowed; }
}
