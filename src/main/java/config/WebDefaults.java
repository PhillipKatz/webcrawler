package config;


import lombok.AccessLevel;
import lombok.Getter;
import lombok.experimental.FieldDefaults;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Getter
//@FieldDefaults(level = AccessLevel.PUBLIC, makeFinal = true)  //not working on "static" since lombok 1.16.16
public class WebDefaults {

    public static final String URL_TO_CRAWL_FROM = "https://www.wikipedia.org";
    public static final String DEPTH_OF_CRAWL = "2";
    public static final List<String> MIME_TYPES_TO_DL = Collections.unmodifiableList(Arrays.asList(
            "text/html"
//            ,     //add anything you want
    ));
    public static final int ROOT_LEVEL = 1;
}
