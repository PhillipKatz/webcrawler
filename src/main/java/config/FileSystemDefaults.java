package config;

public class FileSystemDefaults {

    public static final String CHARSET_USED = "UTF-8";

    private static final String DOWNLOAD_FOLDER_NAME = "dl_pages";
    public static final String DOWNLOAD_FOLDER_LOCATION = getCommonFileLocation(DOWNLOAD_FOLDER_NAME);

    private static final String LOG_FILE_NAME = "url_depths";
    public static final String LOG_FILE_SUFFIX = ".tsv";
    public static final String LOG_FILE_LOCATION = getCommonFileLocation(LOG_FILE_NAME);


    private static final String PROGRESS_FILE_NAME = "progress";
    public static final String PROGRESS_FILE_SUFFIX = ".txt";
    public static final String PROGRESS_FILE_LOCATION = getCommonFileLocation(PROGRESS_FILE_NAME);



    private static String getCommonFileLocation(String fileName) {
        return getFileLocation("crawl_results/" + fileName);
    }

    private static String getFileLocation(String fileName) {
        String currentFolder = System.getProperty("user.dir");
        return currentFolder + "/" + fileName;
    }
}
