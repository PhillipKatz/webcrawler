package file_io;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import config.RuntimeArgs;
import data_structures.ExplorationQueue;
import lombok.SneakyThrows;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FileNotFoundException;

import static config.FileSystemDefaults.CHARSET_USED;

public class ProgramDataFileManager {

    @SneakyThrows
    public static ExplorationQueue getExplorationQueueFromFile() {
        File file = new File(RuntimeArgs.getProgressFilePath());

        ExplorationQueue eq = null;
        try {
            String fileContents = FileUtils.readFileToString(file, CHARSET_USED);
            eq = new Gson().fromJson(fileContents, ExplorationQueue.class);
        } catch (FileNotFoundException e) {
            System.out.println("progress file not found, creating a new queue");
            eq = new ExplorationQueue();
        } catch (JsonSyntaxException e) {
            System.out.println("could not convert the progress file contents to object, creating a new queue");
            eq = new ExplorationQueue();
        }
        return eq;
    }

    @SneakyThrows
    public static void saveExplorationQueueToFile(ExplorationQueue queue) {
        File file = new File(RuntimeArgs.getProgressFilePath());
        String queueJson = new Gson().toJson(queue);
        FileUtils.writeStringToFile(file, queueJson, CHARSET_USED);
    }


}
