package file_io;

import config.RuntimeArgs;
import data_structures.ExplorationQueue;
import data_structures.TSVEntry;
import lombok.SneakyThrows;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.io.FileUtils;
import web.PageExplorer;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.net.URLEncoder;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collection;

import static config.FileSystemDefaults.*;

public class WebpageFileSaver {

    @SneakyThrows
    public static void saveWebFile(String url) {
        String filename = System.currentTimeMillis() + "_" + URLEncoder.encode(url, CHARSET_USED) + ".txt";
        File file = new File(RuntimeArgs.getDownloadDest() + "/" + filename);

        System.out.println("saving contents of url: " + url + "\nInto path: " + file.getAbsolutePath());

        try {
            writeUrlToFile(file, url);
        }
        catch(FileNotFoundException e) {
            //supposedly the filename is too long
            do {
                filename = System.currentTimeMillis() + "";
                file = new File(RuntimeArgs.getDownloadDest() + "/" + filename);
                Thread.sleep(2);
            } while(file.exists()); //don't overwrite another timestamp-file
            writeUrlToFile(file, url);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void writeUrlToFile(File file, String url) throws Exception {
        FileUtils.write(file,
                url +"\n"+ PageExplorer.getWebpage(url).toString(),
                CHARSET_USED);
    }


    @SneakyThrows
    public static void saveCsv(ExplorationQueue queue) {
        String path = RuntimeArgs.getCsvFilePath();
        BufferedWriter writer = Files.newBufferedWriter(Paths.get(path));
        CSVPrinter csvPrinter = new CSVPrinter(writer, CSVFormat.TDF
                .withHeader(TSVEntry.TSVTable.HEADER));  //separated by tabs
        //TODO - recalculate depths
        Collection<TSVEntry> entriesToPutInCsv = queue.getExploredPages().values();
        csvPrinter.printRecords(TSVEntry.TSVTable.getForEntries(entriesToPutInCsv));
        csvPrinter.flush();
    }

    /*

    @SneakyThrows
    public static void saveExplorationQueueToFile(ExplorationQueue queue) {
        File file = new File(RuntimeArgs.getProgressFilePath());
        String queueJson = new Gson().toJson(queue);
        FileUtils.writeStringToFile(file, queueJson, CHARSET_USED);
    }
     */

}
