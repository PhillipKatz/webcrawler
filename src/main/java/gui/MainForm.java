package gui;

import config.RuntimeArgs;
import data_structures.ExplorationQueue;
import file_io.ProgramDataFileManager;
import file_io.WebpageFileSaver;
import lombok.Getter;
import schedueling.Schedueler;
import web.MimeFinder;

import javax.swing.*;

import java.util.Arrays;
import java.util.List;

import static config.FileSystemDefaults.*;
import static config.HardwareLimitations.*;
import static config.WebDefaults.*;
import static web.UrlFormatter.correctIndividualLink;

public class MainForm {
    private JTextField urlField;
    private JButton crawlButton;
    private JButton deriveCSVFromCurrentButton;
    private JTextField depthField;
    private JTextField maxParallelThreadsField;
    private JTextField typesToDownload;
    private JTextField downloadDestField;
    private JTextField csvField;
    private JLabel suffixLabel;
    private JCheckBox reScanUrlLayoutCheckBox;
    private JTextField progressMappingFileField;
    private JLabel progressMappingFileSuffixLabel;
    @Getter
    private JPanel mainPanel;
    private JCheckBox downloadAllResourcesImagesCheckBox;
    private JCheckBox outputTextFeedbackCheckBox;
    private JCheckBox archiveDownloadedPagesCheckBox;

    public MainForm() {

        urlField.setText(URL_TO_CRAWL_FROM);

        depthField.setText(DEPTH_OF_CRAWL);
        maxParallelThreadsField.setText(NUM_OF_PARALLEL_THREADS);

        typesToDownload.setText(String.join(",", MIME_TYPES_TO_DL));

        downloadDestField.setText(DOWNLOAD_FOLDER_LOCATION);

        csvField.setText(LOG_FILE_LOCATION);
        suffixLabel.setText(LOG_FILE_SUFFIX);

        progressMappingFileField.setText(PROGRESS_FILE_LOCATION);
        progressMappingFileSuffixLabel.setText(PROGRESS_FILE_SUFFIX);

        crawlButton.addActionListener(e -> (new Thread(() -> crawl())).start());
        deriveCSVFromCurrentButton.addActionListener(e -> (new Thread(() -> deriveCsvFromCurrentProgress())).start());
    }


    public String getRootUrl() { return urlField.getText(); }
    public Integer getDepthNeeded() { return Integer.valueOf(depthField.getText()); }
    public Integer getMaxParallelThreads() { return Integer.valueOf(maxParallelThreadsField.getText()); }
    public String getDownloadDest() { return downloadDestField.getText(); }
    public String getCsvFilePath() { return csvField.getText() + suffixLabel.getText(); }
    public String getProgressFilePath() { return progressMappingFileField.getText() + progressMappingFileSuffixLabel.getText(); }
    public List<String> getMimeFormatsAllowed() {
        return Arrays.asList(typesToDownload.getText()
                .replaceAll("\\s+","")  //no whitespaces
                .split("\\s*,\\s*"));   //split by commas
    }

    private void crawl() {
        lockButtons();
        updateRuntimeVars();

        ExplorationQueue queueForWork = ProgramDataFileManager.getExplorationQueueFromFile();

        String urlToInvestigate = getRootUrl();
        urlToInvestigate = correctIndividualLink(urlToInvestigate);
        
        //add the given url to queue for handling (ignored if already handled)
        queueForWork.addRootToFringe(urlToInvestigate);

        Schedueler schedueler = new Schedueler(queueForWork);
        schedueler.runCrawlerAsync();

        unlockButtons();
    }

    private void deriveCsvFromCurrentProgress() {
        lockButtons();
        updateRuntimeVars();

        ExplorationQueue queueForWork = ProgramDataFileManager.getExplorationQueueFromFile();
        WebpageFileSaver.saveCsv(queueForWork);

        unlockButtons();
    }


    private void updateRuntimeVars() {
        RuntimeArgs.setCrawlDepth(getDepthNeeded());
        RuntimeArgs.setMaxParallelThreads(getMaxParallelThreads());
        RuntimeArgs.setDownloadDest(getDownloadDest());
        RuntimeArgs.setCsvFilePath(getCsvFilePath());
        RuntimeArgs.setProgressFilePath(getProgressFilePath());
        RuntimeArgs.setMimeFormatsAllowed(getMimeFormatsAllowed());
    }
    private void lockButtons() {changeButtonEnable(false);}
    private void unlockButtons() {changeButtonEnable(true);}
    private void changeButtonEnable(boolean isEnabled) {
        crawlButton.setEnabled(isEnabled);
        deriveCSVFromCurrentButton.setEnabled(isEnabled);
    }
}
