package gui;

import lombok.SneakyThrows;

import javax.swing.*;
import java.awt.*;

public class MainFrame extends JFrame {

    @SneakyThrows
    public MainFrame() {

        setContentPane(new MainForm().getMainPanel());

        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        pack();
        setLocationRelativeTo(null);
        setVisible(true);

    }
}
