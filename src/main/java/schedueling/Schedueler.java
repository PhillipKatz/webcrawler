package schedueling;

import config.RuntimeArgs;
import data_structures.ExplorationQueue;
import data_structures.WebpageStatusEnum;
import file_io.ProgramDataFileManager;
import file_io.WebpageFileSaver;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.val;

import java.util.List;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Data
@RequiredArgsConstructor
public class Schedueler {

    private final ExplorationQueue queue;

    public void runCrawlerAsync() {
        System.out.println("queue at the start of multithreading: " + queue);

        int numOfParallelThreads = RuntimeArgs.getMaxParallelThreads();

        while(!queue.getFringePages().isEmpty()) {
            //don't try it all at once - take a chunk, save. Then next time another chunk... save.

            Set<String> urlsToWorkOn = queue.getFringePages().keySet();
            try {
                CompletableFuture[] futures = urlsToWorkOn.parallelStream()
                        .limit(numOfParallelThreads)
                        .map(url -> CompletableFuture.runAsync(() -> {
                                    try {
                                        handleFringeAddress(url);
                                    } catch (Exception e) {
                                        System.err.println("failed to process url: " + url);
                                        e.printStackTrace();
                                        queue.dumpFringeTaskToFailureList(url);   //maybe don't dump it... turns up this sort of connection issue just happens once in a while
                                    }
                                })
                                        .thenAccept(aVoid -> finishAsync(url))
                        )
                        .toArray(CompletableFuture[]::new);

                CompletableFuture.allOf(futures).join();
            } catch (Exception e) {
                System.err.println("Failed to run current batch of async tasks. Trying again;");
                e.printStackTrace();
            }

            System.out.println("-----------------------------------finished an iteration-----------------------------------");
            //save the progress
            ProgramDataFileManager.saveExplorationQueueToFile(queue);

        }

        System.out.println("done crawling");

        //save the CSV
        WebpageFileSaver.saveCsv(queue);
    }

    private void finishAsync(String url) {
        System.out.println("finished thread that handled: " + url);
    }


    private void handleFringeAddress(String urlToHandle) {
//        int urlDepth = queue.getFringeDepthForUrl(urlToHandle);
        WebpageStatusEnum status = queue.getFringeStatusOfUrl(urlToHandle);
//        System.out.println("handle " + urlToHandle + " (depth: " + urlDepth + ") start - status: " + status);
        System.out.println("starting to handle " + urlToHandle + " at status: " + status);

        switch (status) {
//            case POSSIBLY_NOT_NEW:
//                handlePossiblyNotNew(urlToHandle);
//                break;
            case PENDING_LEGALITY_CHECK:
                handleCheckLinkLegality(urlToHandle);
                break;
            case PENDING_LINK_EXTRACTION:
                handlePendingLinkExtraction(urlToHandle);
                break;
            case FINISHED_EXPLORING:
                handleFinishedExploring(urlToHandle);
                break;
            case PENDING_CONTENT_DOWNLOAD:
                handlePendingContentDownload(urlToHandle);
                break;
            case DONE:
                handleDone(urlToHandle);
                break;

        }
    }

    private void handleCheckLinkLegality(String url) {
        queue.checkLinkLegality(url);
    }

//    private void handlePossiblyNotNew(String url) { queue.checkFringeItem(url); }
    private void handlePendingLinkExtraction(String url) {
        queue.addLinksToTreeAndFringe(url);
    }
    private void handlePendingContentDownload(String url) {

        WebpageFileSaver.saveWebFile(url);
        queue.updateExploredStatus(url);

    }
    private void handleFinishedExploring(String url) { queue.addEntryToExplored(url); }
    private void handleDone(String url) { queue.finishFringeTask(url); }

}
