package web;

import lombok.SneakyThrows;
import lombok.val;
import lombok.var;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class PageExplorer {



    public static List<String> getLegalLinksInWebpage(String webpageUrl) {
        return UrlFormatter.correctHyperlinks(webpageUrl, getLinksInWebpage(webpageUrl));
    }

    public static List<String> getLinksInWebpage(String webpageUrl) {
        Document doc = getWebpage(webpageUrl);

        Elements resultLinks = doc.select("a[href]");

        val legitLinks = resultLinks.parallelStream()
                .map(link -> link.attr("href")) //take the href from every link
                .collect(Collectors.toList());

        System.out.println("found " + legitLinks.size() + " links in: " + webpageUrl);
        return legitLinks;
    }

    //expensive
    @SneakyThrows
    public static Document getWebpage(String webpageUrl) { return Jsoup.connect(webpageUrl).get(); }


    public static double calcSameDomainRatio(String url, List<String> linksWithin) {
        String currentHost = UrlFormatter.getDomain(url);    //TODO - handle exceptions

        if(linksWithin.isEmpty()) {
            System.out.println("no inner links - setting ratio to 0");
            return 0;
        }

        double numOfSameDomain = linksWithin.parallelStream()
                .filter(link -> {
                    try {
                        return isLinkInDomain(currentHost, link);
                    } catch (Exception e) {
                        System.err.println("could not resolve domain,link: " + currentHost + " , " + link);
                        if(!(e instanceof MalformedURLException)) { //don't bother elaborating for malformed urls
                            e.printStackTrace();
                        }
                        return link.contains(currentHost);  //some links are theoretically in the same domain; example: http://sn.wikipedia.org:Wikipedia
                    }
                })
                .count();


        return numOfSameDomain / (double)linksWithin.size();
    }
    @SneakyThrows
    private static boolean isLinkInDomain(String domain, String link) {
        String linkDomain = UrlFormatter.getDomain(link);
        return domain.contains(linkDomain) || linkDomain.contains(domain);  //TODO - improve
    }

}
