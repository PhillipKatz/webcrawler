package web;

import lombok.SneakyThrows;

import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;
import java.util.stream.Collectors;


public class UrlFormatter {

    public static final String CHOSEN_PROTOCOL = "https:";
    public static final String DOMAIN_FORBIDDEN_PREFIX = "www.";

    @SneakyThrows
    public static String getDomain(String address) {
        String domain = new URL(address).getHost();
        return domain.startsWith(DOMAIN_FORBIDDEN_PREFIX) ? domain.substring(DOMAIN_FORBIDDEN_PREFIX.length()) : domain;
    }


    public static String makeHttps(String maybeNotHttps) {
        return maybeNotHttps.replace("http://", "https://");
    }


    /**
     * starts with '#' - it's an internal reference, ignore it, we already have it
     * if starts with "//", append "https:" as prefix
     * handle internal calls (starting with '/') - add the domain as prefix
     */
    public static List<String> correctHyperlinks(String source, List<String> hyperlinks) {
        String correctSource = correctIndividualLink(source);

        return hyperlinks.parallelStream()
                .filter(link -> !link.startsWith("#"))
                .map(link -> correctIndividualLinkRelatively(correctSource, link))
                .map(link -> correctIndividualLink(link))
//                .filter(link -> MimeFinder.isRightMime(link, MIME_TYPES_TO_DL)) //only certain types
                .collect(Collectors.toList());
    }

    public static String correctIndividualLink(String link) {
        if(link.startsWith("//")) {     //missing protocol
            link = CHOSEN_PROTOCOL + link;
        }
        //if still no protocol
        try {
            String firstPart = link.substring(0, link.indexOf("."));
            if (!firstPart.contains("//")) { //if there's no protocol
                link = CHOSEN_PROTOCOL + "//" + link;
            }
        }catch (StringIndexOutOfBoundsException e) {
            System.err.println("There's no '.' in: " + link);
        }
        return link;
    }

//    @SneakyThrows
    private static String correctIndividualLinkRelatively(String domainLink, String link) {

        if(link.startsWith("/") && !link.startsWith("//")) {     //starts with '/' but not with "//" - might be an inner address

//            try {
//                URLConnection con = new URL( link ).openConnection();
////        System.out.println( "orignal url: " + con.getURL() );
//                con.connect();
//                System.out.println( "connected url: " + con.getURL() );
//                InputStream is = con.getInputStream();
//                System.out.println( "redirected url: " + con.getURL() );
//                is.close();
//            } catch (MalformedURLException e) {    //it IS an inner address
//                System.out.println("Adding domain to link: " + link);
                link = getDomain(domainLink) + link;
//            }

        }

        return link;
    }
}
