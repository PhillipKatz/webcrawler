package web;

import data_structures.ExplorationQueue;
import lombok.SneakyThrows;
//import org.apache.http.Header;
//import org.apache.http.HttpResponse;
//import org.apache.http.client.HttpClient;
//import org.apache.http.client.methods.HttpHead;
//import org.apache.http.impl.client.DefaultHttpClient;
//import org.openqa.selenium.By;
//import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.WebElement;
//import org.openqa.selenium.chrome.ChromeDriver;
//import org.openqa.selenium.firefox.FirefoxDriver;

import javax.activation.MimetypesFileTypeMap;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Files;
import java.rmi.server.ExportException;
import java.util.List;

import static web.UrlFormatter.CHOSEN_PROTOCOL;

public class MimeFinder {

    //Expensive operation
    @SneakyThrows
    public static boolean isRightMime(String webpageUrl, List<String> matchingMimes) {
        //only https yields content-type, don't try with http.
        System.out.println("looking up HEAD for url: " + webpageUrl);
        URL url = new URL(webpageUrl);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("HEAD");    //only need the header, don't load the entire page
        connection.connect();   //TODO - find if there's a way to improve the async connections' runtime

        String contentType = connection.getContentType();
        if (contentType == null && !url.getProtocol().equals(CHOSEN_PROTOCOL)) {   //try through https protocol (it isn't https yet)
            String httpsVariant = UrlFormatter.makeHttps(webpageUrl);
            return isRightMime(httpsVariant, matchingMimes);
        } else if (contentType == null) {
            return false;
        }


        //if you reached here - the content-type is fine
        System.out.println("lookup result: " + contentType);

        //Might have extra data (example: "[mime-type], UTF-8"). Only has to start right.
        for (int i = 0; i < matchingMimes.size(); i++) {
            if (contentType.startsWith(matchingMimes.get(i))) {
                return true;
            }
        }

        return false;
    }
}

