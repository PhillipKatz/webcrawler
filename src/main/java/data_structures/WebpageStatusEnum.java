package data_structures;


/**
 * for the workflow:
 * verify that it wasn't already explored (remove if it was) ->
 * find all inner links, store in the tree and correct depths ->
 * calculate ratio and save into explored ->
 * download the contents ->
 * remove from fringe
 */
public enum WebpageStatusEnum {
//    POSSIBLY_NOT_NEW, //cancelled - once you place it in the queue, you overwrite the old one. Check and then enter on next state
    PENDING_LEGALITY_CHECK, //a semi-long process, so it's considered a stage
    PENDING_LINK_EXTRACTION,
    FINISHED_EXPLORING,
    PENDING_CONTENT_DOWNLOAD,
    DONE;

    public WebpageStatusEnum getNext() {
//        if( this == POSSIBLY_NOT_NEW ) return PENDING_LINK_EXTRACTION;
        if( this == PENDING_LEGALITY_CHECK ) return PENDING_LINK_EXTRACTION;
        if( this == PENDING_LINK_EXTRACTION ) return FINISHED_EXPLORING;
        if( this == FINISHED_EXPLORING) return PENDING_CONTENT_DOWNLOAD;
        if( this == PENDING_CONTENT_DOWNLOAD ) return DONE;

        return null;
    }
}
