package data_structures;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import web.PageExplorer;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class TSVEntry {

    final String url;     //one of the 3 requested entries - webpage URL
    Integer depth;     //one of the 3 requested entries - minimal depth of page
    Double ratio;     //one of the 3 requested entries - domain-links ratio
    WebpageStatusEnum status;


    public TSVEntry(String url, Integer depth, WebpageStatusEnum status) {
        this.url = url;
        this.depth = depth;
//        this.ratio = PageExplorer.calcSameDomainRatio(url);
        this.status = status;
    }

    public boolean isFullyDefined() {
        return url != null
                && depth != null
                && ratio != null
                && status != null;
    }

    public void advanceStatus() { this.status = status.getNext(); }


    public static class TSVTable {
        public static final String[] HEADER = {"url", "depth", "ratio", "status"};

        public static List<String[]> getForEntries(Collection<TSVEntry> entries) {
            return entries.parallelStream()
                    .filter(TSVEntry::isFullyDefined)
                    .sorted(Comparator.comparing(TSVEntry::getDepth))
                    .map(entry -> new String[]{entry.url, ""+entry.depth, ""+entry.ratio, entry.status.name()})
                    .collect(Collectors.toList());
        }
    }
}
