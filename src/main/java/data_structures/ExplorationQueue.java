package data_structures;

import config.RuntimeArgs;
import lombok.*;
import lombok.experimental.FieldDefaults;
import web.MimeFinder;
import web.PageExplorer;

import java.util.*;

import static config.WebDefaults.ROOT_LEVEL;
import static data_structures.WebpageStatusEnum.*;

@Data
@FieldDefaults(level = AccessLevel.PROTECTED)
public class ExplorationQueue {

    //url, <depth, ratio>
    final Map<String, TSVEntry> exploredPages = new HashMap<>(); //Already explored. Depth may need an adjustment.

    //url, processing-status ; don't forget to default the status at start of runtime for unfinished pages
    final Map<String, TSVEntry> fringePages = new HashMap<>();   //The outer-nodes that still haven't been explored.

    //url, innerUrls
    final Map<String, List<String>> treeConnections = new HashMap<>();    //once you update the depth of a url, use this mapping to update its children depths
    //TODO - use this after you're done crawling - from the heads (those in depth 1), give all those in the list depth+1 recursively

    final Map<String, TSVEntry> problematicUrls = new HashMap<>();    //for future investigation. Don't let a problematic url here or there ruin everything.



    public WebpageStatusEnum getFringeStatusOfUrl(String url) {
        return fringePages.get(url).getStatus();
    }
    // call only if it's in the fringe for sure!
    public int getFringeDepthForUrl(String url) {
        return fringePages.get(url).getDepth();
    }


    /**
     * call to initialize
     */
    public void addRootToFringe(String url) {
        insertToFringeIfAppropriate(url, ROOT_LEVEL, PENDING_LINK_EXTRACTION);
    }
    private void insertToFringeIfAppropriate(String url, int depth, WebpageStatusEnum status) {
        if (!fringePages.containsKey(url) && !exploredPages.containsKey(url)) {
            addToFringe(url, depth, status);
        } else {  //if possible, lower the depth of the existing links
            val exploredValue = exploredPages.get(url);
            if (exploredValue != null) { exploredValue.setDepth(Math.min(exploredValue.getDepth(), depth)); }
            val fringeValue = fringePages.get(url);
            if (fringeValue != null) { fringeValue.setDepth(Math.min(fringeValue.getDepth(), depth)); }
        }
    }
    private void addToFringe(String url, int depth, WebpageStatusEnum webpageStatusEnum) {
        System.out.println("adding new url to fringe: " + url);
        fringePages.put(url, new TSVEntry(url, depth, webpageStatusEnum));
    }
    private void fringeProgressToNextStage(String url) {
        TSVEntry value = fringePages.get(url);
        value.advanceStatus();
        fringePages.put(url, value);
    }

    /**
     * call on PENDING_LEGALITY_CHECK
     */
    public void checkLinkLegality(String url) {
        if( !MimeFinder.isRightMime(url, RuntimeArgs.getMimeFormatsAllowed()) )
            fringePages.remove(url);
    }

    /**
     * call for FINISHED_EXPLORING
     */
    public void addEntryToExplored(String url) {
        //TODO - calculate the ratio sooner - we're pulling all the links (the entire webpage) in again unjustifiably.
        exploredPages.put(url, fringePages.get(url));
        fringeProgressToNextStage(url);  //next step
    }

    /**
     * call on PENDING_LINK_EXTRACTION
     */
    public void addLinksToTreeAndFringe(String url) {
        List<String> innerLinks = PageExplorer.getLegalLinksInWebpage(url);
        fringePages.get(url).setRatio(PageExplorer.calcSameDomainRatio(url, innerLinks));  //now that we know the inner links, we can calculate the ratio


        innerLinks = new ArrayList<>(new HashSet<>(innerLinks));    //eliminating duplications
        innerLinks.remove(url); //removing current page, in case it points to itself (it might mess-up the registration)

        treeConnections.put(url, innerLinks);
        int fringeLinkDepth = getFringeDepthForUrl(url);

        int newDepth = fringeLinkDepth+1;
        //if within the allowed depth, add-in the next "generation"
        if(newDepth <= RuntimeArgs.getCrawlDepth()) {
            innerLinks.parallelStream().forEach(link -> insertToFringeIfAppropriate(link, newDepth, PENDING_LINK_EXTRACTION));
        }

        fringeProgressToNextStage(url);  //next step
    }

    /**
     * call after PENDING_CONTENT_DOWNLOAD
     */
    public void updateExploredStatus(String url) {
        fringeProgressToNextStage(url);  //next step
    }

    /**
     * call for DONE
     */
    public void finishFringeTask(String url) {
        exploredPages.get(url).setStatus(DONE);
        fringePages.remove(url);
    }

    /**
     * call for failed hyperlink connections
     */
    public void dumpFringeTaskToFailureList(String url) {
        val valueRemoved = fringePages.remove(url);
        problematicUrls.put(url, valueRemoved);
    }

}
