***WebCrawler***
---------------------

Notice:
- The depth is calculated so:
    * You can add a new root at any run. If you keep working with previously saved scans, your new crawl will merge with the previous.
    * The newly added root is always considered lowest-depth. It WILL update the depths of all other logged links.
- The inner-domain-links/all-links ratio is calculated with these in mind:
    * Inner-page references ("#some-indexing-link") are discarded, not being taken into account
    * Duplicate links heighten are taken into account
- web-pages are explored in this way:
    * Unless specified otherwise, the protocol used is https.
    * If a link-lookup fails on http, it will be attempted again with https.
- Downloaded web-pages' file-name constraints:
    * Every file-name is saved in this format: [timestamp]_[url-in-%-safe-characters].
    * Every file's contents' first row is its path. The rest is its http contents.
    * If a url is too long to save (a constraint of the OS), the file-name would be shortened to 
- This program can be improved-upon:
    * I've left some "TODOs" in the code. Those are extra functionalities that I didn't manage to complete in time...
    * There are several run-time optimizations left to make
    * Several exceptions aren't handled
    * Feedback to user isn't featured yet (for now, use the run-terminal...)

If you get any trouble running it in "Intellij IDEA",
please download and install the Lombok plugin.
Make sure to let the IDE process the annotations pre-processing,
so that you wouldn't get any false errors. Guide: https://stackoverflow.com/questions/41161076/adding-lombok-plugin-to-intellij-project


If your problems persist, please contact me via email.

-Phillip